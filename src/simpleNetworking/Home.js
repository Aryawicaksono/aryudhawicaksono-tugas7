import {Text, View, FlatList, Image, TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';
import Icon from 'react-native-vector-icons/AntDesign';
import {TOKEN, BASE_URL} from './url';
import {useIsFocused} from '@react-navigation/native';

export default function Home({navigation}) {
  const isFocused = useIsFocused();

  useEffect(() => {
    getDataMobil();
  }, [isFocused]);

  const [dataMobil, setDataMobil] = useState('');

  const getDataMobil = async () => {
    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
      });

      const result = await response.json();
      console.log('Success:', result);
      setDataMobil(result.items);
    } catch (error) {
      console.error('Error 1234:', error);
    }
  };

  const convertCurrency = (nominal = 0, currency) => {
    let rupiah = '';
    const nominalref = nominal.toString().split('').reverse().join('');
    for (let i = 0; i < nominalref.length; i++) {
      if (i % 3 === 0) {
        rupiah += nominalref.substr(i, 3) + '.';
      }
    }

    if (currency) {
      return (
        currency +
        rupiah
          .split('', rupiah.length - 1)
          .reverse()
          .join('')
      );
    } else {
      return rupiah
        .split('', rupiah.length - 1)
        .reverse()
        .join('');
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Text
        style={{fontWeight: 'bold', fontSize: 20, margin: 15, color: '#000'}}>
        Home screen
      </Text>
      <FlatList
        data={dataMobil}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <View>
            <TouchableOpacity
              onPress={() => navigation.navigate('AddData', item)}
              activeOpacity={0.8}
              style={{
                width: '90%',
                alignSelf: 'center',
                marginTop: 15,
                borderColor: '#dedede',
                borderWidth: 1,
                borderRadius: 6,
                padding: 12,
                flexDirection: 'row',
              }}>
              <View
                style={{
                  width: '30%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  style={{width: '90%', height: 100, resizeMode: 'contain'}}
                  source={{uri: item.unitImage}}
                />
              </View>
              <View
                style={{
                  width: '70%',
                  paddingHorizontal: 10,
                }}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                    Nama Mobil :
                  </Text>
                  <Text style={{fontSize: 14, color: '#000'}}>
                    {' '}
                    {item.title}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                    Total KM :
                  </Text>
                  <Text style={{fontSize: 14, color: '#000'}}>
                    {' '}
                    {item.totalKM}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                    Harga Mobil :
                  </Text>
                  <Text style={{fontSize: 14, color: '#000'}}>
                    {' '}
                    {convertCurrency(item.harga, 'Rp. ')}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        )}
      />
      <TouchableOpacity
        style={{
          position: 'absolute',
          bottom: 30,
          right: 10,
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: 'red',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() => navigation.navigate('AddData')}>
        <Icon name="plus" size={20} color="#fff" />
      </TouchableOpacity>
    </View>
  );
}
